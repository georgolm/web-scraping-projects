# Web scraping the best 100 Songs from radioeins.de
# The 100 best songs will be voted by a jury. 
# Every person votes the ten best songs for each category
# The vote for each Person will be public at 9 am at https://www.radioeins.de/musik/die-100-besten-2019/
# My target is to build a reproducible web scraper, scraping the individual votes and calculate the top 100 myself. 

library(rvest)
library(tidyverse)
library(magrittr)
library(stringdist)

#Defining Functions ####
#Function for getting votes from a judge
get_df <- function(input){
  tryCatch(
    {
      output <- read_html(paste("https://www.radioeins.de", input, sep = "")) %>% 
        html_nodes("table") %>% 
        html_table() %>% 
        extract2(2) %>% 
        as.data.frame()
      
      return(output)
    },
    
    error = function(e){
      print(i)
    }
  )
}

#Function for calculating the difference between songs/artists 
dist_to_df <- function(inDist) {
  if (class(inDist) != "dist") stop("wrong input type")

  A <- attr(inDist, "Size")
  B <- if (is.null(attr(inDist, "Labels"))) sequence(A) else attr(inDist, "Labels")
  
  if (isTRUE(attr(inDist, "Diag"))) attr(inDist, "Diag") <- FALSE
  if (isTRUE(attr(inDist, "Upper"))) attr(inDist, "Upper") <- FALSE
  
  data.frame(
    row = B[unlist(lapply(sequence(A)[-1], function(x) x:A))],
    col = rep(B[-length(B)], (length(B)-1):1),
    value = as.vector(inDist))
}

#Getting the Mode of a list
Mode <- function(x) {
  ux <- unique(x)
  ux[which.max(tabulate(match(x, ux)))]
}

'%notin%' <- Negate('%in%')

#Setting up URL for scraping
radio1_best100 <- read_html("https://www.radioeins.de/musik/die-100-besten-2019/")
radio1_url <- "https://www.radioeins.de"

best100_topic <- radio1_best100 %>% 
  html_nodes(".manualteaserTitle .uebersicht") %>% 
  html_text() %>% 
  as.vector()

best100_href= radio1_best100 %>% 
  html_nodes(".manualteaserTitle .uebersicht") %>% 
  html_attr("href") %>% 
  as.vector()

Overview_df <- data.frame(Topic = best100_topic[1:7],
                          r1_url = radio1_url,
                          path = best100_href)

### change this part on sunday ####

rows <- seq(0,16, by = 1)
rows[18] <- 430458397


data = vector(mode = "character")

# Testing for varying jury members // optional code ####

data_prev <- as.character(seq(1:98))

# for (i in 1:50) {
# 
#   data <- read_html("https://www.radioeins.de/musik/die-100-besten-2019/staedtesongs/") %>%
#     html_nodes(".containerMain .beitrag") %>% # reading this data, there is a curious thing, Patrick kessler is present/or not on different website reloads
#     html_attr("href") %>%
#     unique() %>%
#     str_subset("/musik/")
# 
#   new_jury <- data[data %notin% data_prev]
#   print(new_jury)
# 
#   data_prev <-  data
# 
# }

# Scraping Data ####
# Jury-Members 

data <- read_html("https://www.radioeins.de/musik/die-100-besten-2019/staedtesongs/") %>% 
  html_nodes(".containerMain .beitrag") %>% 
  html_attr("href") %>% 
  unique() %>%     
  str_subset("/musik/")

#Individual Votes 
Results_df <- data.frame(place = numeric(),
                         artist = character(),
                         song = character(),
                         jury_member = character(),
                         stringsAsFactors = FALSE)
jury_ColName <- names(Results_df)[-4]
exception <- vector(mode = "numeric")

for (i in 1:length(data)) {
  jury_i <- get_df(data[i])
  
  if (class(jury_i) == "data.frame") {
    
    names(jury_i) <- jury_ColName  
    
    if(length(names(jury_i)) != length(jury_ColName)){ #detecting inconsistency in judge names
      outlier <- read_html(paste("https://www.radioeins.de", data[i], sep = "")) %>% 
        html_nodes(".TitleText") %>% 
        html_text() %>% 
        print()
      
      jury_i <- jury_i[,jury_ColName]
    }
    
    jury_i$jury_member <- read_html(paste("https://www.radioeins.de", data[i], sep = "")) %>% 
      html_nodes(".TitleText") %>% 
      html_text()
    Results_df <- rbind(Results_df, jury_i)
    
  }else{
    exception <- append(exception, jury_i)
  }
}

for (j in 1:length(exception)) {
  
  jury_j <- read_html(paste("https://www.radioeins.de", data[exception[j]], sep = "")) %>% 
    html_nodes("table") %>%
    html_table(fill = TRUE) %>% #fill = TRUE
    extract2(3) %>% 
    as.data.frame()
  
  names(jury_j) <- jury_ColName  
  jury_j$jury_member <- read_html(paste("https://www.radioeins.de", data[exception[j]], sep = "")) %>% 
    html_nodes(".TitleText") %>% 
    html_text()
  Results_df <- rbind(Results_df, jury_j)
}

#sapply(Results_df, function(x) sum(is.na(x)))

Results_df$place <- as.numeric(Results_df$place)
Results_df_nona <- na.omit(Results_df)
Results_df_nona$score = 0

# Implementing competition rules ####
place_dict = c(1,2,3,4,5,6,7,8,9,10)
score_dict = c(12,10,8,7,6,5,4,3,2,1)
for (i in 1:length(place_dict)) {
  Results_df_nona[Results_df_nona$place == place_dict[i],5] = score_dict[i]
}

Results_df_nona$song_dirty = Results_df_nona$song
Results_df_nona$artist_dirty = Results_df_nona$artist

# Handlin inconsistency in names of songs/artists ####
Results_df_nona$song = gsub("(Live)","", Results_df_nona$song)
Results_df_nona$song = gsub(" and ", " & ", Results_df_nona$song, ignore.case = TRUE)
Results_df_nona$song = sub("The ", "",  Results_df_nona$song) #! maybe the same with "A"
Results_df_nona$song = gsub("\n  ", "", Results_df_nona$song) 
#Results_df_nona$song = gsub(" and ", " & ", Results_df_nona$song, ignore.case = TRUE)
Results_df_nona$song = gsub("\\s","", Results_df_nona$song, fixed = FALSE) 
Results_df_nona$song = gsub("’|‘|´|'|,|`|\\.|-","", Results_df_nona$song, fixed = FALSE) # new line
Results_df_nona$artist = gsub(" and ", " & ", Results_df_nona$artist, ignore.case = TRUE)
Results_df_nona$artist = gsub("The ", "",  Results_df_nona$artist, ignore.case = TRUE)
Results_df_nona$artist = gsub("\\s","", Results_df_nona$artist, fixed = FALSE) 
Results_df_nona$artist = gsub("’|‘|´|'|,|`|\\.|-","", Results_df_nona$artist, fixed = FALSE) # new line

# Summarising data #### 
Charts <- Results_df_nona %>% 
  mutate(song = tolower(song),
         artist = tolower(artist)) %>% 
  group_by(artist, song) %>% 
  summarise(avg_place = mean(place),
            score = sum(score),
            mentioned = n(),
            song_dirty = Mode(song_dirty),
            artist_dirty = Mode(artist_dirty))


#kable(head(Charts,25), "html")

#Charts$song_sim <- paste(Charts$artist, Charts$song)

# Fuzzy Matching ####
d_song = stringdistmatrix(Charts$song)
d_artist = stringdistmatrix(Charts$artist)
song_sim <- dist_to_df(d_song) %>% 
  rename(song = value)
song_sim$artist <- dist_to_df(d_artist)$value

song_sim <- song_sim %>% 
  filter(song <= 2 & artist <= 2) %>% 
  arrange(song, artist) %>% 
  mutate(artist_row = 0,
         artist_col = 0,
         song_row = 0,
         song_col = 0,
         score_row = 0,
         score_col = 0,
         place_row = 0,
         place_col = 0)

for (i in 1:nrow(song_sim)) {
    song_sim[i,"artist_row"] = Charts[song_sim[i,1],"artist"]
    song_sim[i,"artist_col"] = Charts[song_sim[i,2],"artist"]
    song_sim[i,"score_row"] = Charts[song_sim[i,1],"score"]
    song_sim[i,"score_col"] = Charts[song_sim[i,2],"score"]
    song_sim[i,"song_row"] = Charts[song_sim[i,1],"song"]
    song_sim[i,"song_col"] = Charts[song_sim[i,2],"song"]
    song_sim[i,"mentioned_row"] = Charts[song_sim[i,1],"mentioned"]
    song_sim[i,"mentioned_col"] = Charts[song_sim[i,2],"mentioned"]
}
song_sim$new_score = rowSums(song_sim[,9:10])

double_vec = vector(mode = "numeric")
for (i in 1:nrow(song_sim)) {
  if (song_sim[i,"score_row"] > song_sim[i,"score_col"]) {
    Charts[song_sim[i,1],"score"] = Charts[song_sim[i,1],"score"] + Charts[song_sim[i,2],"score"]
    Charts[song_sim[i,1],"mentioned"] = Charts[song_sim[i,1],"mentioned"] + Charts[song_sim[i,2],"mentioned"]
    double_vec = append(double_vec, song_sim[i,2])
  } else {
    Charts[song_sim[i,2],"score"] = Charts[song_sim[i,1],"score"] + Charts[song_sim[i,2],"score"]
    Charts[song_sim[i,2],"mentioned"] = Charts[song_sim[i,1],"mentioned"] + Charts[song_sim[i,2],"mentioned"]
    double_vec = append(double_vec, song_sim[i,1])
  }
}

Charts_clean <- Charts[-double_vec,]%>% 
  arrange(desc(score), desc(mentioned), artist, song) %>% 
  rowid_to_column("place") %>% 
  mutate(song = song_dirty,
         artist = artist_dirty) %>% 
  select(place, artist, song, score, mentioned, avg_place)



